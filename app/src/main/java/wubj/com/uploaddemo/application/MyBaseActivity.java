package wubj.com.uploaddemo.application;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import java.util.Map;

import wubj.com.uploaddemo.R;


/**
 * Wubj 创建于 2017/5/16.
 */
public class MyBaseActivity extends AppCompatActivity implements DialogControl {

    protected Context mContext;
    protected ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 添加Activity到堆栈
        mContext = this;
        mProgress = getWaitDialog(mContext, getString(R.string.loading));
//        mProgress = SuccinctProgress.getSuccinctProgress(mContext, getString(R.string.loading));
        mProgress.setCancelable(true);
        mProgress.setCanceledOnTouchOutside(false);

        getIntentData();
        initView();
        initData();
        initListener();
    }

    protected void getIntentData() {
    }

    /**
     * 初始化视图
     */
    public void initView(){
    }

    public void initData() {
    }

    public void initListener() {
    }

    @SuppressWarnings("unchecked")
    public <T> T $(int id) {
        return (T) this.findViewById(id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onEventMainThread(Map<String, Object> inParam) {
    }

    public void showProgress(String text) {
        if (mProgress != null && !mProgress.isShowing()) {
            if (!TextUtils.isEmpty(text)) {
                setMessage(text);
            }
            mProgress.show();
        }
    }

    private void setMessage(String msg) {
        mProgress.setMessage(msg);
    }

    /**
     * 不显示圈圈
     */
    private void dismissProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    @Override
    public void hideWaitDialog() {
        dismissProgress();
//        SuccinctProgress.dismiss();
    }

    @Override
    public void showWaitDialog() {
        showProgress("");
//        SuccinctProgress.showSuccinctProgress(mContext, getString(R.string.loading));
    }

    @Override
    public void showWaitDialog(int resourcesId) {
        showProgress(getString(resourcesId));
//        SuccinctProgress.showSuccinctProgress(mContext, getString(resourcesId));
    }

    @Override
    public void showWaitDialog(String text) {
        showProgress(text);
//        SuccinctProgress.showSuccinctProgress(mContext, text);
    }

    public ProgressDialog getWaitDialog(Context context, String message) {
        ProgressDialog waitDialog = new ProgressDialog(context);
        if (!TextUtils.isEmpty(message)) {
            waitDialog.setMessage(message);
        }
        return waitDialog;
    }
}
