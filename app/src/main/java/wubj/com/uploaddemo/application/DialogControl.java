package wubj.com.uploaddemo.application;

public interface DialogControl {

    void hideWaitDialog();

    void showWaitDialog();

    void showWaitDialog(int resid);

    void showWaitDialog(String text);
}
