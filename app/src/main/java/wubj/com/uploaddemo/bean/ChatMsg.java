/**
 * Copyright (C) 2014 xspace app Project.
 */
package wubj.com.uploaddemo.bean;

import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @version V1.0.0
 * @ClassName: package-info
 * @author:mawen
 * @Date�?014-5-20
 * @Description: 聊天消息详情
 */
public class ChatMsg implements Serializable {
    private static final long serialVersionUID = 8042317157714267699L;
    public int _id;
    // 内容
    public String mContent;
    // �?��内容类型 0：文�?1、图�?2、语�?3、视�?
    public int mContentType;
    //0:图像,1:语音,2:视频,4:其他,5:附件
    public String mFileType;


    // 发�?时间
    public String mSendTime;
    // 入库时间
    public String mInsertTime;
    // 消息状�? 0：发送中 1:发�?成功 2 发�?失败 3接收
    public int mStatus = 0;
    // 消息状�? 0：未�?1:已读
    public int mRead = 1;
    // 发�?消息的barejid 对方账号
    public String toUser;
    // 发�?消息的uuid
    public String mUUID;
    // 用户�?
    public String mToUserName;
    // 拥有�?自己账号
    public String fromUser;
    public String fromUserName;
    public String fromUserLogo;
    public String errorCode;// 错误�?
    public String errorMsg;// 错误消息
    public String imgPath;//图片本地存储路径
    public String mobile;
    public boolean isForward = false;

    public String myuserid;
    public String uploadUrl;//上传url
    public String submitUrl;//提交url
    public long msgId;
    public String fileName;
    public long size;
    public String fileType;
    public transient Drawable apkIoc = null;
    public int fileState = 0;//0:等待上传,1:正在上传，2：上传完成 ,3:上传失败

    public int progress;
    public long uploaded;
    public long total;

    public String picShootTime;
    /**
     * 0 单聊 1群组
     **/
    public int messageType;
    /**
     * 区分环信的聊天和邀请信息
     **/
    public String platForm;
    public boolean isGroup = false;
    // 存储扩展字段
    public HashMap<String, String> mExHM = new HashMap<String, String>();


}
