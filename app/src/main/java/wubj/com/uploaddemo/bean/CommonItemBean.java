package wubj.com.uploaddemo.bean;

/**
 * Wubj 创建于 2017/3/20 0020.
 */
public class CommonItemBean {

    public CommonItemBean(String title, int tag) {
        this.title = title;
        this.tag = tag;
    }

    public CommonItemBean(int picRes, String title, int tag) {
        this.picRes = picRes;
        this.title = title;
        this.tag = tag;
    }

    public int tag;
    public int picRes;
    public String title;
    /**
     * popupWindows使用
     * 只显示未读状态,不显示数字,则使用unreadStatus
     * 显示数字.则使用unreadCount
     */
    public boolean unreadStatus;//是否显示未读小红点
    public int unreadCount;
}
