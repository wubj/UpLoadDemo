package wubj.com.uploaddemo.bean;

import java.io.Serializable;

public class MediaBean implements Serializable {
    /**
     * serialVersionUID :
     */
    private static final long serialVersionUID = 1L;

    public String mediaName;

    /**
     * 本地路径
     */
    public String content;
    /**
     * 0 img  1voice 2视频 4 5 附件
     **/
    public String file_type;
    public String file_url;
    public String file_name;
    //后缀名
    public String content_type;
    public String file_id;
    public String path_type;// 0 :本地路径 1: 网络路径
    /**
     * 传附件时作为文件的insertTime存储
     * 某些地方作为语音的时长使用,以后语音时长使用voiceTime和timeLenth
     */
    public String insert_time;
    //拍摄的图片使用
    public String pic_shoot_time = "";
    //附件使用
    public String file_size = "";//文件大小
    //音频文件使用
    public String voiceTime = "";
    public String file_dutime = "";
    public int timeLength;
    /**
     * 1上传成功
     */
    public String file_state;

    public boolean isShowDelete = false;

    public MediaBean() {
        super();
    }

    public MediaBean(String disparentName, String content, String content_type) {
        super();
        this.mediaName = disparentName;
        this.content_type = content_type;
        this.content = content;
    }

    public MediaBean(String mediaName, String voiceTime, String content,
                     String content_type) {
        super();
        this.mediaName = mediaName;
        this.voiceTime = voiceTime;
        this.content_type = content_type;
        this.content = content;
    }
}
