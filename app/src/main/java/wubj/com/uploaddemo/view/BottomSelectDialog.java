package wubj.com.uploaddemo.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import wubj.com.uploaddemo.R;
import wubj.com.uploaddemo.bean.CommonItemBean;


/**
 * Wubj 创建于 2017/1/11 0027.
 */
public class BottomSelectDialog extends Dialog {

    private Context mContext;
    //    private List<String> mList = new ArrayList<>();
    private List<CommonItemBean> mList = new ArrayList<>();
    private BottomAdapter adapter;

    private ISelectItemListener mListener;

    public interface ISelectItemListener {
        void clickListener(View view, CommonItemBean bean, int position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setGravity(Gravity.BOTTOM);
        WindowManager m = getWindow().getWindowManager();
        Display d = m.getDefaultDisplay();
        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.width = d.getWidth();
        getWindow().setAttributes(p);
    }

    private BottomSelectDialog(Context mContext, int theme) {
        super(mContext, theme);
        this.mContext = mContext;
        View view = View.inflate(mContext, R.layout.dialog_bottom_select, null);
        findView(view);
        super.setContentView(view);
    }

    private void findView(View view) {
        try {
            MyListView listView = (MyListView) view.findViewById(R.id.lv_bottom_select);
            adapter = new BottomAdapter();
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (mListener != null) {
                        mListener.clickListener(view, mList.get(position), position);
                    }
                    dismiss();
                }
            });
            RelativeLayout mLayout_cancel = (RelativeLayout) view.findViewById(R.id.layout_bottom_select_cancel);
            mLayout_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public BottomSelectDialog setSingleChoiceItems(String[] arrays, ISelectItemListener listener) {
//        List<String> list = Arrays.asList(arrays);
//        setSingleChoiceItems(list, listener);
//        return this;
//    }

//    public BottomSelectDialog setSingleChoiceItems(List<String> list, ISelectItemListener listener) {
//        mList.addAll(list);
//        this.mListener = listener;
//        return this;
//    }

    public BottomSelectDialog setSingleChoiceItems(List<CommonItemBean> list, ISelectItemListener listener) {
        mList.addAll(list);
        this.mListener = listener;
        return this;
    }

    @Override
    public void show() {
        adapter.notifyDataSetChanged();
        super.show();
    }

    public static class Builder {
        BottomSelectDialog bottomSelectDialog;

        public Builder(Context context, int theme) {
            bottomSelectDialog = new BottomSelectDialog(context, theme);
        }

        public Builder(Context context) {
            this(context, R.style.CustomDialog);
        }

        public BottomSelectDialog build() {
            return bottomSelectDialog;
        }

    }

    private class BottomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public CommonItemBean getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = viewHolder.holderView;
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.bindData(getItem(position));
            return convertView;
        }
    }

    class ViewHolder {
        View holderView;
        TextView textView;

        public ViewHolder() {
            holderView = initHolderView();
            holderView.setTag(this);
        }

        public View initHolderView() {
            View view = View.inflate(mContext, R.layout.item_dialog_bottom_select, null);
            textView = (TextView) view.findViewById(R.id.tv_bottom_select_item);
            return view;
        }

        public void bindData(CommonItemBean bean) {
            textView.setText(bean.title);
        }
    }
}
