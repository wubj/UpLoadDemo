package wubj.com.uploaddemo.upload;

public class DownConstants {

    public static final int START_STATE = -2;// Not started download

    public static final int START = -1;// Waiting download

    public static final int DOWNLOADING = 0;// Downloading

    public static final int PAUSED = 1;// Suspend download

    public static final int COMPLETE = 2;// Finish download

    public static final int CANCELLED = 3;// Cancel download
	
    public static final int ERROR = 4;// Error download

    public static final int TIME_OUT = 30 * 1000; // Connect network time out

    public static final int MAX_BUFFER_SIZE = 1024*16; // Every time to get maximum amount of data
	
    public static int taskMaxNumber = 1;//At the same time download task size
    
    public static boolean reseedFlag = false;
    
}
