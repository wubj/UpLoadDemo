package wubj.com.uploaddemo.upload;

import java.util.ArrayList;

import wubj.com.uploaddemo.bean.ChatMsg;

public class UploadMg {
    public static UploadMg upload;
    private UploadManager mUploadManager;

    private UploadMg() {
        mUploadManager = new UploadManager();
    }

    public static UploadMg getInstance() {
        if (null == upload) {
            upload = new UploadMg();
        }
        return upload;
    }

    public void uploadFile(ArrayList<ChatMsg> mList, IuploadListener listener) {
        ChatListAidl uploadList = new ChatListAidl(mList);
        mUploadManager.addTask(uploadList, listener);
    }
}