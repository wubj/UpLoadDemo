package wubj.com.uploaddemo.upload;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import wubj.com.uploaddemo.bean.ChatMsg;

public class ChatListAidl implements Parcelable {
    public ArrayList<ChatMsg> respList;

    public ChatListAidl(ArrayList<ChatMsg> respList) {
        super();
        this.respList = respList;

    }

    public ChatListAidl(Parcel in) {
        readFromParcel(in);

    }

    protected void readFromParcel(Parcel in) {
        respList = (ArrayList<ChatMsg>) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeSerializable(respList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatListAidl> CREATOR = new Creator<ChatListAidl>() {
        public ChatListAidl createFromParcel(Parcel in) {
            return new ChatListAidl(in);
        }

        public ChatListAidl[] newArray(int size) {
            return new ChatListAidl[size];
        }
    };
}
