package wubj.com.uploaddemo.upload;

import wubj.com.uploaddemo.bean.ChatMsg;

public interface IuploadListener {

    void onUploadBefore(ChatMsg uploads);

    void onUploading(int position, long uploaded, long total, float progress, float speed, ChatMsg uploads, int
            current);

    void onUploadError(int position, long uploaded, float progress, ChatMsg uploads, int current);

    void onUploadAllFinished(int position, ChatMsg uploads, boolean checkFlag);

    void onCancelUpload(int position, ChatMsg uploads, int current);

    void onPauseUpload(int position, ChatMsg uploads, int current);

    void onResumeUpload(int position, ChatMsg uploads, int current);
}
