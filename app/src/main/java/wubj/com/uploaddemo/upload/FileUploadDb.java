package wubj.com.uploaddemo.upload;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import java.util.ArrayList;

import wubj.com.uploaddemo.bean.ChatMsg;


/**
 * MessageDb数据库
 */
public class FileUploadDb extends BaseDb {

    public FileUploadDb(Context context) {
        super(context);
    }

    public static final String TAG = "FileUploadDb";
    public static final String _ID = "_id";
    public static final String FROM_UUID = "from_uuid";
    public static final String TO_UUID = "to_uuid";
    public static final String UPLOAD_FILE_INFO = "upload_file_info";
    public static final String MSG_ID = "msg_id";

    public static final String TABLE_NAME = "FILE_UPLOAD_INFO";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
            + " (" + _ID + " INTEGER primary key," + FROM_UUID + " CHAR(128),"
            + TO_UUID + " CHAR(128)," + MSG_ID + " LONG," + UPLOAD_FILE_INFO
            + " BLOB)";

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    /**
     * 保存消息数据
     *
     * @return
     */
    public int insertOrUpdateMsg(ChatMsg info) {
        ContentValues values = new ContentValues();
        values.put(FROM_UUID, info.fromUser);
        values.put(TO_UUID, info.toUser);
        values.put(UPLOAD_FILE_INFO, getBytesFromObject(info));
        values.put(MSG_ID, info.msgId);

        String sql = "select * from " + TABLE_NAME + " where " + MSG_ID + " = "
                + info.msgId;
        Cursor cursor = null;
        try {
            long rowId = -1;
            checkDb();
            cursor = db.rawQuery(sql, null);
            if (cursor != null && cursor.getCount() > 0) {
                rowId = db.update(TABLE_NAME, values, MSG_ID + "=? ",
                        new String[]{"" + info.msgId});
            } else {
                rowId = db.insert(TABLE_NAME, null, values);
                Log.e(TAG, "insertOrUpdateMsg()... insert rowId: " + rowId);
            }
            return (int) rowId;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            closeDbAndCursor(cursor);
        }
    }

    /**
     * 分页查询数据
     *
     * @return
     */
    public ArrayList<ChatMsg> findMsgList(String fromUserId, String toUserId) {
        ArrayList<ChatMsg> dataList = new ArrayList<ChatMsg>();
        String sql = "select * from " + TABLE_NAME + " where " + FROM_UUID
                + " ='" + fromUserId + "' and " + TO_UUID + " ='" + toUserId
                + "'order by " + MSG_ID;
        System.out.println("--------------------------sql:" + sql);
        Cursor cursor = null;
        try {
            checkDb();
            cursor = db.rawQuery(sql, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ChatMsg chatMsgSnap = (ChatMsg) getObjectFromBytes(cursor
                            .getBlob(cursor.getColumnIndex(UPLOAD_FILE_INFO)));
                    dataList.add(chatMsgSnap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDbAndCursor(cursor);
        }
        Log.d(TAG, sql + " , and find " + dataList.size());

        return dataList;
    }

    public void clearAllData() {
        try {
            checkDb();
            String sql = "delete from " + TABLE_NAME + ";";
            db.execSQL(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteMsg(String fromUserId, String toUser) {
        try {
            checkDb();
            String whereClause = String.format(" %s = ? and %s = ? ",
                    FROM_UUID, TO_UUID);
            String[] whereArgs = new String[]{fromUserId, toUser};
            int res = db.delete(TABLE_NAME, whereClause, whereArgs);
            return res > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteMsg(long msgId) {
        try {
            checkDb();
            String whereClause = String.format(" %s = ?", MSG_ID);
            String[] whereArgs = new String[]{"" + msgId};
            int res = db.delete(TABLE_NAME, whereClause, whereArgs);
            return res > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
